const moment = require('moment');
const fs = require('fs');
const path = require('path');

// Function to process JSON data
function dateShiftBankingData(bankfeedsJson) {
  // 2017 data version
  const momentNow = moment();
  bankfeedsJson['submissionTime'] =
    momentNow.format('YYYY-MM-DD') + 'T12:53:00';
  const bankAccounts = bankfeedsJson['bankData']?.['bankAccounts'];
  const moment6MonthsAgo = momentNow.clone().add(-6, 'months');
  // last transaction date is later than the search end date 🤷‍♀️
  let firstTransactionMoment = moment6MonthsAgo.clone().add(2, 'days');
  let lastTransactionMoment = momentNow.clone().add(1, 'days');

  bankAccounts[0]['statementSummary']['searchPeriodStart'] =
    moment6MonthsAgo.format('YYYY-MM-DD');
  bankAccounts[0]['statementSummary']['searchPeriodEnd'] =
    momentNow.format('YYYY-MM-DD');
  bankAccounts[0]['statementSummary']['transactionsStartDate'] =
    firstTransactionMoment.format('YYYY-MM-DD');
  bankAccounts[0]['statementSummary']['transactionsEndDate'] =
    lastTransactionMoment.format('YYYY-MM-DD');

  let dayEndBalances = bankAccounts[0]['dayEndBalances'].map(
    (dayEndBalance, index) => {
      const momentDay = momentNow.clone().add(index * -1, 'days'); // descending date
      return { ...dayEndBalance, date: momentDay.format('YYYY-MM-DD') };
    },
  );

  bankAccounts[0]['dayEndBalances'] = dayEndBalances;

  let bankTransactions = bankAccounts[0]['transactions'].map(
    (transaction, index) => {
      // keep transactions dates to within 6 months
      const diff = Math.min(index, 180);
      const momentDay = momentNow.clone().add(diff * -1, 'days');
      return { ...transaction, date: momentDay.format('YYYY-MM-DD') };
    },
  );
  bankAccounts[0]['transactions'] = bankTransactions;

  // Yep there are three bank accounts and I want to keep their date ranges consistent
  firstTransactionMoment = moment6MonthsAgo.clone().add(1, 'days');
  lastTransactionMoment = momentNow.clone().add(1, 'days');
  bankAccounts[1]['statementSummary']['searchPeriodStart'] =
    moment6MonthsAgo.format('YYYY-MM-DD');
  bankAccounts[1]['statementSummary']['searchPeriodEnd'] =
    momentNow.format('YYYY-MM-DD');
  bankAccounts[1]['statementSummary']['transactionsStartDate'] =
    firstTransactionMoment.format('YYYY-MM-DD');
  bankAccounts[1]['statementSummary']['transactionsEndDate'] =
    lastTransactionMoment.format('YYYY-MM-DD');

  // reusing the variable, careful how you edit this
  dayEndBalances = bankAccounts[1]['dayEndBalances'].map(
    (dayEndBalance, index) => {
      const momentDay = momentNow.clone().add(index * -1, 'days'); // descending date
      return { ...dayEndBalance, date: momentDay.format('YYYY-MM-DD') };
    },
  );

  bankAccounts[1]['dayEndBalances'] = dayEndBalances;

  bankTransactions = bankAccounts[1]['transactions'].map(
    (transaction, index) => {
      // keep transactions dates to within 6 months
      const diff = Math.min(index, 180);
      const momentDay = momentNow.clone().add(diff * -1, 'days');
      return { ...transaction, date: momentDay.format('YYYY-MM-DD') };
    },
  );
  bankAccounts[1]['transactions'] = bankTransactions;

  // Yep there are three bank accounts and I want to keep their date ranges consistent
  firstTransactionMoment = moment6MonthsAgo.clone().add(1, 'days');
  lastTransactionMoment = momentNow.clone().add(1, 'days');
  bankAccounts[2]['statementSummary']['searchPeriodStart'] =
    moment6MonthsAgo.format('YYYY-MM-DD');
  bankAccounts[2]['statementSummary']['searchPeriodEnd'] =
    momentNow.format('YYYY-MM-DD');
  bankAccounts[2]['statementSummary']['transactionsStartDate'] =
    firstTransactionMoment.format('YYYY-MM-DD');
  bankAccounts[2]['statementSummary']['transactionsEndDate'] =
    lastTransactionMoment.format('YYYY-MM-DD');

  // reusing the variable, careful how you edit this
  dayEndBalances = bankAccounts[2]['dayEndBalances'].map(
    (dayEndBalance, index) => {
      const momentDay = momentNow.clone().add(index * -1, 'days'); // descending date
      return { ...dayEndBalance, date: momentDay.format('YYYY-MM-DD') };
    },
  );

  bankAccounts[2]['dayEndBalances'] = dayEndBalances;

  bankTransactions = bankAccounts[2]['transactions'].map(
    (transaction, index) => {
      // keep transactions dates to within 6 months
      const diff = Math.min(index, 180);
      const momentDay = momentNow.clone().add(diff * -1, 'days');
      return { ...transaction, date: momentDay.format('YYYY-MM-DD') };
    },
  );
  bankAccounts[2]['transactions'] = bankTransactions;

  return bankfeedsJson;
}

// Main function to read, process, and write the file
function main() {
  // Check if a file path argument is provided
  if (process.argv.length < 3) {
    console.error('Usage: node script.js <file-path>');
    process.exit(1);
  }

  // Get the file path from the command line arguments
  const filePath = process.argv[2];

  // Read the JSON file
  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error(`Error reading file: ${err}`);
      process.exit(1);
    }

    try {
      // Parse the JSON data
      let jsonData = JSON.parse(data);

      // Process the data
      jsonData = dateShiftBankingData(jsonData);

      // Create the updated file name
      const parsedPath = path.parse(filePath);
      const updatedFilePath = path.join(
        parsedPath.dir,
        `${parsedPath.name}_${moment().format('YYYYMMDD')}${parsedPath.ext}`,
      );

      // Write the processed data to the new file
      fs.writeFile(
        updatedFilePath,
        JSON.stringify(jsonData, null, 2),
        'utf8',
        err => {
          if (err) {
            console.error(`Error writing file: ${err}`);
            process.exit(1);
          }
          console.log(`File has been updated and saved as: ${updatedFilePath}`);
        },
      );
    } catch (err) {
      console.error(`Error parsing JSON: ${err}`);
      process.exit(1);
    }
  });
}

// Run the main function
main();
