# Download, extract and make
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
make

# Test build
make test

# Install to correct locations
sudo make install

# Things should work now, you can check by running (in seperate terminals):
	# redis-server
	# redis-cli ping


# The following is from the section titled "Installing Redis more properly" @ https://redis.io/topics/quickstart

sudo mkdir /etc/redis
sudo mkdir /var/redis

sudo cp utils/redis_init_script /etc/init.d/redis_6379
sudo cp redis.conf /etc/redis/6379.conf

sudo mkdir /var/redis/6379

echo 'go to https://redis.io/topics/quickstart and check what values to update /etc/redis/6379.conf with'
echo 'then run the commented out portions of the script'

# sudo update-rc.d redis_6379 defaults
# sudo /etc/init.d/redis_6379 start