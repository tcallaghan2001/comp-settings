/* eslint-disable no-console */
// eslint-disable-next-line import/no-extraneous-dependencies
const chalk = require("chalk");
const mongoose = require("mongoose");
const fs = require("fs");

const main = async () => {
	await mongoose.connect("mongodb://localhost:27017/sail-local");

	const migrationSchema = new mongoose.Schema(
		{
			lastRun: String,
			previousLastRun: String,
			migrations: [
				{
					title: String,
					timestamp: Number,
				},
			],
		},
		{
			timestamps: {
				createdAt: "created_at",
				updatedAt: "updated_at",
			},
		}
	);

	const Migration = mongoose.model("Migration", migrationSchema);
	const [{ migrations }] = await Migration.find();
	const migrationsLookup = migrations.reduce((acc, m) => {
		acc[m.title.split(".")[0]] = !!m.timestamp;
		return acc;
	}, {});

	let hasMissingMigrations = false;
	const files = fs.readdirSync(
		"/home/tadhg/Data/code/lumi_server/apps/all/src/migration/migrations"
	);
	files.forEach((file) => {
		const migrationName = file.split(".")[0];
		const found = migrationsLookup[migrationName];
		if (!found) {
			if (hasMissingMigrations === false) {
				console.warn("");
				console.warn(
					`${chalk.bold.red("Migrations you have not run:")}`
				);
				hasMissingMigrations = true;
			}
			console.warn(`\t${chalk.bold.red(`${migrationName}`)}`);
		}
		delete migrationsLookup[migrationName];
	});

	const remainingMigrations = Object.keys(migrationsLookup);
	if (remainingMigrations.length > 0) {
		console.warn("");
		console.warn(
			`${chalk.bold.yellow(
				"Migrations you have ran, but are no longer required:"
			)}`
		);
		remainingMigrations.forEach((remainingMigration) =>
			console.warn(`\t${chalk.bold.yellow(`${remainingMigration}`)}`)
		); // eslint-disable-line function-paren-newline
	}

	if (hasMissingMigrations) {
		process.exit(1);
	} else {
		console.warn(`
    ${chalk.bold.green("Migrations are up to date")}
`);
		process.exit(0);
	}
};

main()
	.catch(console.error)
	.finally(() => process.exit(1));
