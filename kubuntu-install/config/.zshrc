# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# User configuration

# Load the "before" shell dotfiles
# .local can be used for other settings you don’t want to commit
for file in ~/.extras/before/.*(.); do
	if [[ -f "$file" ]] && [[ -r "$file" ]]; then
		source "$file"
	fi
done
unset file


# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(ssh-agent zsh-nvm z thefuck zsh-autosuggestions zsh-syntax-highlighting zlong_alert)

source $ZSH/oh-my-zsh.sh

# Load any "after" shell dotfiles
# .local can be used for other settings you don’t want to commit
for file in ~/.extras/after/.*(.); do
	if [[ -f "$file" ]] && [[ -r "$file" ]]; then
		source "$file"
	fi
done
unset file

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# https://github.com/junegunn/fzf
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
