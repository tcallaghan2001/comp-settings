#!/usr/bin/env bash

# Custom commands: create a file (with executable access) called 'git-[command]' in the $PATH

src="$1"
msg="$2"

is_branch() {
  git show-ref --verify --quiet "refs/heads/$src"
}

is_commit_reference() {
  git rev-parse --verify --quiet "$src" > /dev/null 2>&1
}

is_on_current_branch() {
  local commit_sha=`git rev-parse "$src"`
  git rev-list HEAD |
    grep -q -- "$commit_sha"
}

commit_if_msg_provided() {
  if test -n "$msg"; then
    git commit --amend -m "$msg"
  fi
}

prompt_continuation_if_squashing_master() {
  if [[ $src =~ ^master$ ]]; then
    read -p "Warning: squashing '$src'! Continue [y/N]? " -r
    if ! [[ $REPLY =~ ^[Yy]$ ]]; then
      echo "Exiting"
      exit 1
    fi
  fi
}

squash_branch() {
  prompt_continuation_if_squashing_master
  git merge --squash "$src" || exit 1  # quits if `git merge` fails
  commit_if_msg_provided
}

squash_current_branch() {
  git reset --soft "$src" || exit 1    # quits if `git reset` fails
  commit_if_msg_provided
}

if [[ $src = "-h" || $src = "--help" ]]; then
	printf "Give the hash you want to squash up to\nThis hash will now contain all commits from head to supplied hash (and any new commit message)\n";
elif `is_branch`; then
  squash_branch
elif `is_commit_reference` && `is_on_current_branch`; then
  squash_current_branch
else
  echo "Source branch or commit reference required." 1>&2 && exit 1
fi
