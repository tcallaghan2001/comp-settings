[user]
	email = tadhgocallaghan@domain.com
	name = "Tadhg O'Callaghan"



[core]
	excludesfile = ~/.gitignore
	hooksPath = ~/GitHooks/
	autocrlf = false
	pager = less -S -F
	editor = nano



[alias]
	co = checkout
	com = checkout master
	cod = checkout development
	coe = !git checkout $EPIC
	cof = !git checkout $FEATURE
	merge-force = merge --no-ff

	# rebase with latest changes and fetch all other changes
	up = !git pull --rebase --prune $@ && git fetch --all

	# checkout branch or create it first if necessary
	cob = "!f() { git checkout $1 2>/dev/null || git checkout -b $1; }; f"

	# commit + message
	cm = !git add -A && git commit -q -m

	# last commit message
	lcm = !git --no-pager show -s --pretty='%C(yellow)%h %C(reset)- %C(white)%s %C(reset)'

	# update the previous commit (changing the message)
	amend = !git add -A && git commit --amend --reuse-message=HEAD

	# create temporary commit (use with undo or amend)
	save = !git add -A && git commit --no-verify -m 'SAVEPOINT'

	# don't hard reset without a marker, just in case
	wipe = !git add -A && git commit --no-verify -qm 'WIPE SAVEPOINT' && git reset HEAD^ --hard


	# short status
	st = !git add -A && git status --short --branch
	s = !git st


	# pretty log
	gl		= log --graph --branches --remotes --tags --pretty=format:'%C(yellow)%h %C(reset)- %C(auto)%D %C(white)%<(60,trunc)%s %C(green)(%cr) %C(reset)<%an>'
	gla		= log --graph --branches --remotes --tags --pretty=format:'%C(yellow)%h %C(reset)- %C(auto)%D %C(white)%s %C(green)(%cr) %C(reset)<%an>'
	gl1		= !git log "$@" --graph --pretty=format:'%C(yellow)%h %C(blue)- %C(auto)%D %C(white)%<(60,trunc)%s %C(green)(%cr) %C(reset)<%an>'
	gl1a	= !git log "$@" --graph --pretty=format:'%C(yellow)%h %C(blue)- %C(auto)%D %C(white)%s %C(green)(%cr) %C(reset)<%an>'
	gla1	= !git gl1a

	# pretty reflog
	reflogger = reflog --pretty=format:'%C(yellow)%h %C(reset)- %C(auto)%D %C(white)%s %C(green)(%cr) %C(reset)<%an>'


	# search
	search = !git gl --grep

	# most updated files
	churn = !git log --all -M -C --name-only --format='format:' "$@" | sort | grep -v '^$' | uniq -c | sort -n -r | awk 'BEGIN {print "count,file"} {print $1 "," $2}'

	# who's most active
	contributors = shortlog --summary --numbered

	# list branches by commit date
	overview = branch --list --all --sort=-committerdate --format='%(HEAD) %(color:yellow)%(objectname:short)%(color:reset) - %(color:white)%(refname:short)%(color:reset) %(color:green)(%(committerdate:relative))%(color:reset) <%(authorname)>'

	# list local only branches - https://stackoverflow.com/a/31776247/1195949
	local-branches = "!git branch -a --format='%(refname:short) %(upstream:short)' | awk '{if (!$2) print $1;}'"
	lb = local-branches

	# search branches
	# branch-search = !git branch --all --remote --sort=-committerdate | grep
	branch-search = "!f() { git branch --all --sort=-committerdate --list "*${1}*" ; }; f"
	bs = branch-search

	# used to simplify other aliases
	meld  = "!f() { git add -A; git difftool -d $@ >/dev/null 2>&1; }; f"
	meld2 = "!f() { git add -A; git difftool --diff-filter=R $@ >/dev/null 2>&1; }; f"
	bname = rev-parse --abbrev-ref HEAD


	# diff with meld (dr (diff renames) leaves our dir diffing as this messes up showing rename diffs)
	d  = "!f() { git meld   ${1-$(git bname)} $2; git st; }; f"
	dr = "!f() { git meld2  ${1-$(git bname)} $2; git st; }; f"
	du = d @{upstream}
	de = !git d $EPIC
	dd = !git d development
	dc = "!f() { \
		echo ""; \
		echo "Commits on ${COLOR_GREEN}${1-$(git bname)}${COLOR_RESET} that are not on ${COLOR_RED}${2-development}${COLOR_RESET}"; \
		echo ""; \
		git log --oneline --no-merges ${1-$(git bname)} ^${2-development}; \
	}; f"

	# diff latest commit to it's parent (accepts params, "2" for second latest, "3" for third latest etc)
	dl = "!f() { num=$((${1-1}-1)); echo "$(git lcm HEAD~$(($num+1))) ↔️_ $(git lcm HEAD~$num)"; git meld HEAD~$(($num+1)) HEAD~$num; }; f"

	# diff hash with parent
	dp = "!f() { echo "$(git lcm ${1-HEAD}^) ↔️_ $(git lcm ${1-HEAD})" && git meld "${1-HEAD}"^.."${1-HEAD}"; }; f"

	# diff hash with working directory
	dw= !echo "$(git lcm $1)" && git meld
	dh = !git dw

	# merge with meld
	m = mergetool

	# rebase current branch on specified branch (defaults to development)
	# need to update this to check if the branch has been pushed and stop if it has
	refresh = 		"!f() { currentBranch=$(git bname); git co ${1-development} && git up && git co $currentBranch && git rebase ${1-development}; }; f"
	refresh-merge = "!f() { currentBranch=$(git bname); git co ${1-development} && git up && git co $currentBranch && git merge-force ${1-development}; }; f"

	# fixup history
	fixup = rebase --interactive
	#squash = "!f() { num=$1; msg=${2-$(git lcm HEAD)}; echo "squashing up to excluding $(git lcm HEAD~$num)"; git reset --soft HEAD~$num && git add -A && git commit -m "$msg"; }; f"
	#squash = "!f() { num=$1; msg=${2-$(git lcm HEAD)}; echo "start $msg end"; git reset --soft HEAD~$num && git add -A && git commit -m"$msg"; }; f"
	#temp = "!bash -ic git-squash $@"

	# move unpushed commits to a new branch
	move = "!f() { \
		if [ -z $(git status --porcelain) ]; then \
			currentBranch=$(git bname); \
			git branch ${1-feature1} \
			&& git reset --quiet --hard origin/$currentBranch \
			&& git co ${1-feature1}; \
		else \
			echo ""; \
			echo '**** DIRTY ****'; \
			echo "Commit changes before moving, or use git cob if no previous commits to move"; \
		fi; \
	}; f"

	# https://stackoverflow.com/a/39776107/1195949
	# will ignore/unignore changes to the passed in file
	ignore = update-index --skip-worktree
	unignore = update-index --no-skip-worktree

	deleteBranch = db

	parent = "!f() { git log -$(git rev-list --no-walk --count ${1-HEAD}^@) $(git rev-parse ${1-HEAD}^@) --pretty=format:'%h - %D %s (%cr) <%an>' | cat; }; f"
	parent2 = "!f() { git rev-parse ${1-HEAD}^@; }; f"

	# list aliases
	la = !git config -l | grep alias | sed -l 20 's/alias\\.\\([^=]*\\)=\\(.*\\)/\\1\\ \t => \\2/' | sort | less -S



[push]
	default = simple
	followTags = true # why would I not want to push tags?



[merge]
	# force fast-forward style merge
	ff = only
	conflictstyle = diff3



[status]
	showUntrackedFiles = all



[transfer]
	fsckobjects = true



[diff]
	tool = meld
[difftool "meld"]
	cmd = meld "$LOCAL" -L older "$REMOTE" -L newer
[difftool "diffmerge"]
	cmd = sgdm "$LOCAL" "$REMOTE"  --title1="older" --title2="newer"
[difftool "kdiff3"]
	cmd = kdiff3 "$LOCAL" -L older "$REMOTE" -L newer
[difftool "vscode"]
  cmd = code --wait --new-window --diff $LOCAL $REMOTE
[difftool]
	prompt = false


[merge]
	tool = "meld"
[mergetool "meld"]
	cmd = meld "$LOCAL" -L RemoteRebase "$MERGED" -L MERGED "$REMOTE" -L LocalRebase --output "$MERGED"
[mergetool "diffmerge"]
	path = sgdm
[mergetool "kdiff3"]
	cmd = kdiff3 $BASE $LOCAL $REMOTE -o $MERGED
[mergetool "vscode"]
  cmd = code --wait --new-window $MERGED
[mergetool]
	keepBackup = false


[pull]
	ff = only


[maintenance]
	repo = /home/tadhg/Data/code/comp-settings
	repo = /home/tadhg/Data/code/lumi_server_2
