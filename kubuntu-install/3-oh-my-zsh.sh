#!/bin/zsh

# This needs to be run as a zsh script


# Install oh-my-zsh
# See: https://github.com/robbyrussell/oh-my-zsh
sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
