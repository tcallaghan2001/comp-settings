#!/bin/zsh

# This needs to be run as a zsh script

highlight=$(tput setaf 1)
reset=$(tput sgr0)

# Need to get access to $ZSH_CUSTOM
source ~/.zshrc

# Install plugins
git clone https://github.com/lukechilds/zsh-nvm $ZSH_CUSTOM/plugins/zsh-nvm
git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
git clone https://github.com/kevinywlui/zlong_alert.zsh.git $ZSH_CUSTOM/plugins/zlong_alert
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

# Install thefuck (plugin comes preinstalled, but missing the application)
# See: https://github.com/nvbn/thefuck
sudo apt-get -y install python3-dev python3-pip
sudo pip3 install thefuck

# Setup the various linked files
mkdir $ZSH_CUSTOM/themes
ln -sf $PWD/config/gitconfig.txt ~/.gitconfig
ln -sf $PWD/config/gitignore.txt ~/.gitignore
ln -sf $PWD/config/GitHooks ~/
ln -sf $PWD/config/.zshrc ~/.zshrc
ln -sf $PWD/config/.extras ~/

# Reload profile file and run some commands
source ~/.zshrc
ln -sf $PWD/config/default-packages ~/.nvm/default-packages
nvm install 'lts/*'
nvm alias default 'lts/*'
echo "${highlight}############################################################${reset}"
echo "${highlight}CHECK NODE FOR DEFAULT PACAKGES, THE ABOVE LINK MAY NOT WORK${reset}"
echo "${highlight}Create link to code folder, (if not already at ~/code)${reset}"
echo "${highlight}  e.g ln -sfr /home/tadhg/Data/code ~/${reset}"
echo "${highlight}############################################################${reset}"

# Change git repo to ssh
git remote set-url origin git@bitbucket.org-tcallaghan2001:tcallaghan2001/comp-settings.git

# This stops super+p from switching monitor input,
# see https://askubuntu.com/a/1287582/616199
gsettings set org.gnome.mutter.keybindings switch-monitor "[]"
