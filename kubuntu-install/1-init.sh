#!/bin/sh

# Get comp-settings repo, don't run if already downloaded
if [ -f "./2-apps-install.sh" ]; then
	echo 'repo already exists, aborting'
else
	git clone https://bitbucket.org-tcallaghan2001/tcallaghan2001/comp-settings.git
fi
