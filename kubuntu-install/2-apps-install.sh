#!/bin/sh

# Useful: https://www.ubuntuupdates.org/
# This can be run as a bash script


highlight=$(tput setaf 1)
reset=$(tput sgr0)


## ADD KEYS ##
echo "${highlight}adding keys${reset}"

# Chrome
echo "${highlight}adding chrome${reset}"
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -

# VS Code
echo "${highlight}adding vscode${reset}"
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg

# Yarn
echo "${highlight}adding yarn${reset}"
wget -q -O - https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -

# pgAdmin4
echo "${highlight}adding pgAdmin4${reset}"
wget -q https://www.pgadmin.org/static/packages_pgadmin_org.pub -O- | sudo apt-key add -

# # Terminator
# # This is a guess, will probably break, try second one if it fails, then fallback to below
# echo "${highlight}adding terminator${reset}"
# sudo apt-key add ./ppa-keys/gnome-terminator_ubuntu_ppa.gpg
# # sudo apt-key add ./ppa-keys/gnome-terminator_exported.key





## ADD REPOSITORIES ##
echo "${highlight}adding repositories${reset}"

# Chrome
echo "${highlight}adding chrome${reset}"
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'

# VS Code
echo "${highlight}adding code${reset}"
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg

# Yarn
echo "${highlight}adding yarn${reset}"
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

# pgAdmin4
echo "${highlight}adding pgAdmin4${reset}"
sudo add-apt-repository "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main"

# Git
echo "${highlight}adding git${reset}"
sudo add-apt-repository ppa:git-core/ppa -y

# # Terminator
# echo "${highlight}adding terminator - experimental${reset}"
# echo "deb http://ppa.launchpad.net/gnome-terminator/ppa/ubuntu xenial main" | sudo tee /etc/apt/sources.list.d/gnome-terminator.list
# # sudo add-apt-repository ppa:gnome-terminator -y
# # https://askubuntu.com/questions/940658/the-repository-http-ppa-launchpad-net-gnome-terminator-ppa-ubuntu-yakkety-rel
# # basically this will fail and in software & updates you will need to select the repository and change <disribution> to xenial

# KeepassXC
echo "${highlight}adding KeepassXC${reset}"
sudo add-apt-repository ppa:phoerious/keepassxc -y




## UPDATE SOURCES ##
echo "${highlight}updating sources${reset}"
sudo apt-get update
sudo apt-get -y dist-upgrade



## INSTALL ##
echo "${highlight}installing apps${reset}"
echo "${highlight}NOTE: if any one is not valid, none will install${reset}"
cat ./apps-list | xargs sudo apt-get -y install



## MISC ##
echo "${highlight}copying terminator config${reset}"
mkdir ~/.config/terminator
ln -sf $PWD/config/terminator.config ~/.config/terminator/config

echo "${highlight}copying keepass config${reset}"
mkdir ~/.config/keepassxc
ln -sfr ./config/keepassxc.ini ~/.config/keepassxc/keepassxc.ini

echo "${highlight}copying launchers${reset}"
# snap files: /var/lib/snapd/desktop/applications
# ubuntu files: /usr/share/applications
ln -sfr ./config/custom-launchers ~/.local/share/applications

# Not sure if this worked. I may have forgotten to restart to verify if it worked first. I ran it manually and restarted and it worked
echo "${highlight}changing default shell to zsh (this will need a comp restart to take effect)${reset}"
chsh -s $(which zsh)




## CREATE SSH KEYS ##
echo "${highlight}creating ssh keys${reset}"
ssh-keygen -q -N '' -f ~/.ssh/id_rsa
ssh-keygen -q -N '' -f ~/.ssh/tcallaghan2001
echo "created at ~/.ssh/id_rsa.pub and ~/.ssh/tcallaghan2001.pub"
echo "${highlight}copying ssh config${reset}"
ln -sfr ./config/ssh.config ~/.ssh/config
