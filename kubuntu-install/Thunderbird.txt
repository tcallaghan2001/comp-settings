install thunderbird
	- setup: https://uit.stanford.edu/service/office365/configure/thunderbird
	- no longer need to copy to Sent Items, done automatically
	- configure folders: https://kb.wisc.edu/office365/page.php?id=28427
		- this seems to be included in the first link, leaving in case it changes in future

add lightning add on (through normal addon)

add Exchange EWS Provider (local file)
	- download xpi from https://github.com/ExchangeCalendar/exchangecalendar/releases
	- follow instructions https://github.com/Ericsson/exchangecalendar/wiki/How-to-Add-Your-Calendar-to-Thunderbird
	- can get settings from owa (settings cog, mail link, pop and imap)
