Create shortcut in Startup (e.g. %userprofile%\Start Menu\Programs\Startup)

Right click the shortcut and in Target put (including quotes):
"C:\Program Files\AutoHotkey\AutoHotkey.exe" "C:\Code\bitbucket\scripts\AutoHotKey\Startup.ahk"

In Start in put (including quotes):
"C:\Code\bitbucket\scripts\AutoHotKey"