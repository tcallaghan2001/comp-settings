﻿#IfWinActive ahk_class rctrl_renwnd32
{
	^g::
	^LButton::
	{
		BlockInput, on
		prevClipboard = %clipboard%
		clipboard =
		Send, ^c
		BlockInput, off
		ClipWait, 2

		if ErrorLevel = 0
		{
			matchedText = ''
			copiedText = %clipboard%
			clipboard = %prevClipboard%

			if (RegExMatch(copiedText, "iS)(smb:\/\/[\w \-\/\\\(\)\+]*\.?[A-Za-z0-9]{0,4})", matchedText))
			{
				StringReplace, matchedText, matchedText, smb:, , All
				StringReplace, matchedText, matchedText, /, \, All
			}
			else if (RegExMatch(copiedText, "S)(JIRA \([A-Z]*-\d*\)?)", matchedText))
			{
				StringReplace, matchedText, matchedText, JIRA (, , All
				StringReplace, matchedText, matchedText, ), , All
				matchedText = firefox.exe https://jira.9msn.net/jira/browse/%matchedText%
			}
			else if (RegExMatch(copiedText, "iS)((http|https)://[\S]+)", matchedText))
				or (RegExMatch(copiedText, "iS)(www\.\S+)", matchedText))
				or (RegExMatch(copiedText, "iS)(\w+\.(com.au|com|net|org|gov|cc|edu|info))", matchedText))
				or (RegExMatch(copiedText, "iS)([a-zA-Z]:\\{1,2}[\w \-\/\\\(\)\+]*\.?[A-Za-z0-9]{0,4})", matchedText))
				or (RegExMatch(copiedText, "iS)(\\\\[\w \-\/\\\(\)\+]*\.?[A-Za-z0-9]{0,4})", matchedText))
			{
				; no manipulation needed
			}

			try
			{
				Run, %matchedText%
			}
			catch
			{
				MsgBox, Sorry, I don't know what to do with this:`n`n%matchedText%
			}
		}

		return
	}
}

#IfWinNotActive ahk_class SunAwtFrame
{
	^!t::
	{
		if WinExist("ahk_class VirtualConsoleClass")
			WinActivate
		else
			run ConEmu64.exe -Dir C:\Code\
		return
	}
}