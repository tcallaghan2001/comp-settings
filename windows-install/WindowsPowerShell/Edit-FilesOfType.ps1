##############################################################################
#.SYNOPSIS
# Searches recursively files matching the given input
# 	strFileType:	Extension to search for
##############################################################################
function Edit-FilesOfType
{
	Param
	(
		[string] $strFileType
	)
	Process
	{
		Get-ChildItem -recurse -include "*.$strFileType" | % { &"C:\Program Files (x86)\Notepad++\notepad++.exe" $_.FullName }
	}
};
