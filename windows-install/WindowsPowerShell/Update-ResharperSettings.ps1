##############################################################################
#.SYNOPSIS
# Reads template file from current location and copies to all subfolders where a .sln file exists
##############################################################################
function Update-ResharperSettings
{
		$templateFile = Get-ChildItem "*.DotSettings"
		Get-ChildItem -recurse -include "*.sln" | % {
			Copy-Item $templateFile.FullName  $_.FullName.Replace("sln", "sln.DotSettings")
			Write-Host "Updated file:" $_.FullName.Replace("sln", "sln.DotSettings")
		}
};
