##############################################################################
#.SYNOPSIS
# Searches recursively for any bin or ojb folders and deletes
# 	DeleteFiles:	Doesn't delete files unless set to true. Defaults to false
##############################################################################
function Remove-BuildItems
{
	Param
	(
		[string] $DeleteFiles = $false
	)
	Process
	{
		switch ($DeleteFiles)
		{
			$true { Get-ChildItem -include bin,obj -recurse -force | foreach-object { remove-item $_.fullname -force -recurse }; }
			$false { Get-ChildItem -include bin,obj -recurse -force | foreach-object { remove-item $_.fullname -force -recurse -whatif }; }
		}
	}
};
