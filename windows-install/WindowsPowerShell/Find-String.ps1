##############################################################################
#.SYNOPSIS
# Searches all matching files recursively for the given string
# 	strInputString:	Accepts regular expressions
# 	strFileType:	Accept wildcards. Default *.*
##############################################################################
function Find-String
{
	Param
	(
		[string]$strInputString = '',
		[string]$strFileType = '*.*'
	)
	Process
	{
		 Get-ChildItem -recurse -include $strFileType | ? { $_ -notmatch ".hg" } |
			Select-String $strInputString |
				Group-Object Path |
					Select-Object Count, @{Name='Location'; expression={if ($_.Name.Length -gt 96) { $_.Name.Substring($_.Name.Length - 96) } else { $_.Name }}}, @{Expression={ $_.Group | foreach { $_.LineNumber} }; Name="Line Numbers"} |
						Sort-Object Count -Descending |
							Format-Table -autosize -wrap
	}
};
Set-Item -path alias:find -value Find-String;
