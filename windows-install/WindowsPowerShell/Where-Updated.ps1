##############################################################################
#.SYNOPSIS
# Searches recursively for all files updated between dates
# 	minDate:	Defaults to today (midnight local time)
# 	maxDate:	Defaults to now (local time)
##############################################################################
function Where-Updated
{
	Param
	(
		[DateTime]$minDate = [DateTime]::Today,
		[DateTime]$maxDate = [DateTime]::Now
	)
	Process
	{
		Get-ChildItem -recurse | ? { $_ -notmatch ".hg" } |
			? { ( ($_.Extension -ne ".dll") -and ($_.Extension -ne ".pdb") -and ($_.Extension -ne ".resources") -and ($_.Extension -ne ".cache") -and ($_.Extension -ne ".exe") -and ($_.LastWriteTime -ge $minDate) -and ($_.LastWriteTime -lt $maxDate)) } |
				Select-Object @{Name='Location'; expression={if ($_.FullName.Length -gt 119) { $_.FullName.Substring($_.FullName.Length - 119) } else { $_.FullName }}}
					Format-Table
	} 
};
Set-Item -path alias:wu -value Where-Updated;
