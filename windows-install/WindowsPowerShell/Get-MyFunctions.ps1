##############################################################################
#.SYNOPSIS
# Lists all custom functions
##############################################################################
function Get-MyFunctions
{
	Get-ChildItem function: | where { $sysfunctions -notcontains $_ }
}
Set-Item -path alias:MyFunctions -value Get-MyFunctions;
Set-Item -path alias:Functions -value Get-MyFunctions;
Set-Item -path alias:Funcs -value Get-MyFunctions;
Set-Item -path alias:Get-CustomFunctions -value Get-MyFunctions;
Set-Item -path alias:CustomFunctions -value Get-MyFunctions;