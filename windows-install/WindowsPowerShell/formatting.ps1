Import-Module PSColor
Start-SshAgent -Quiet

Set-Theme -name 'Paradox'
$global:PSColor.File.Code.Pattern  = '\.(java|c|cpp|cs|js|coffee|css|html)$'

Set-Alias z j

if ((Get-Location).Path -eq "C:\Users\$env:username") { Set-Location D:\code\ }
