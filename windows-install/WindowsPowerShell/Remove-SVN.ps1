##############################################################################
#.SYNOPSIS
# Searches recursively for any .svn folders and deletes
# 	DeleteFiles:	Doesn't delete files unless set to true. Defaults to false
##############################################################################
function Remove-SVN
{
	Param
	(
		[bool] $DeleteFiles = $false
	)
	Process
	{
		switch ($DeleteFiles)
		{
			$true { Get-ChildItem -include .svn -recurse -force | Remove-Item -recurse -force; }
			$false { Get-ChildItem -include .svn -recurse -force | Remove-Item -recurse -force -whatif; }
		}
	}
};
