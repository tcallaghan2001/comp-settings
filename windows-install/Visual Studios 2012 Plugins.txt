Visual Studio (2012)
	AnkhSVN - Subversion Support for Visual Studio
	AutoHistory
	Git Source Control Provider (need to set this in options also)
	Mindscape Web Workbench
	Productivity Power Tools (assuming using with R# then only enable the following)
		Colorized Parameter Help
		Color Printing
		Custom Document Well
		Enhanced Scroll Bar
		Fix Mixed Tabs
		Middle-click scrolling
		Power Commands
		Quick Launch Tasks
	Spell Checker
	Web Essentials
