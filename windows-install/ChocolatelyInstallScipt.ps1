param (
	[switch]$shutdown = $false
)

# confirm user is administrator
$user = [Security.Principal.WindowsIdentity]::GetCurrent();
$isAdmin = (New-Object Security.Principal.WindowsPrincipal $user).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
if ($isAdmin -eq $false) {
	Write-Host
	Write-Host "**************************************" -ForegroundColor darkred
	Write-Host "*****                            *****" -ForegroundColor darkred
	Write-Host "***** NOT ADMINISTRATOR. EXITING *****" -ForegroundColor darkred
	Write-Host "*****                            *****" -ForegroundColor darkred
	Write-Host "**************************************" -ForegroundColor darkred
	Write-Host
	Exit
}

# confirm user doesn't want to shutdown the PC
if ($shutdown -eq $false) {
	Write-Host
	Write-Host "*********************************************************" -ForegroundColor darkred
	Write-Host "*****                                               *****" -ForegroundColor darkred
	Write-Host "***** PC will not shutdown after this has completed *****" -ForegroundColor darkred
	Write-Host "*****                                               *****" -ForegroundColor darkred
	Write-Host "***** Click Ctrl + c to quit and run with -shutdown *****" -ForegroundColor darkred
	Write-Host "*****                                               *****" -ForegroundColor darkred
	Write-Host "*********************************************************" -ForegroundColor darkred
	Write-Host
	# wait to give a chance to cancel script
	start-sleep 30
}

# need to close these apps before updating
Write-Host
Write-Host "**********************************" -ForegroundColor darkred
Write-Host "*****                        *****" -ForegroundColor darkred
Write-Host "***** Save and close keepass *****" -ForegroundColor darkred
Write-Host "*****                        *****" -ForegroundColor darkred
Write-Host "**********************************" -ForegroundColor darkred
Write-Host

# get chocolatey
iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))

# disable prompts
choco feature enable -n allowGlobalConfirmation

# install browsers
cinst GoogleChrome
cinst Firefox
cinst Opera
cinst safari

# install essentials
cinst nodist
cinst notepadplusplus.install
cinst 7zip.install
cinst clipx --allow-empty-checksums
cinst autohotkey.install --allow-empty-checksums
cinst keepass
cinst dropbox
cinst paint.net # this doesn't seem to be working
cinst gimp
cinst diffmerge --allow-empty-checksums # this doesn't seem to be working

# some others
# cinst libreoffice
# cinst foxitreader
cinst virtualbox # this doesn't seem to be working
cinst windirstat
cinst fiddler4
#cinst linqpad4
cinst procexp
cinst procmon
cinst putty.install
cinst filezilla
cinst clover # this doesn't seem to be working
cinst lili
cinst slack

# IDEs
cinst atom
cinst brackets
cinst sublime --allow-empty-checksums
cinst webstorm
cinst pgadmin3
cinst conemu

# source control
#cinst hg
#cinst tortoisehg
cinst git.install
#cinst git-credential-winstore
cinst gitextensions
cinst tortoisegit
cinst tortoisesvn
cinst sourcetree

# plugins
# cinst javaruntime
# cinst flashplayerplugin

# just for me
cinst spotify
cinst calibre
cinst jbs

if ($shutdown) {
	# wait 10 minutes and shutdown
	start-sleep 600
	stop-computer
}
