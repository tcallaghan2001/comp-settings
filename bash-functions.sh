
# find files and overwrite with text
find . -name ".npmrc" -exec bash -c 'echo "registry = \"http://npm-9msn.9pub.io\"" > "$1"' _ {} \;

# delete matching files (or directories)
( find . -type d -name ".git" && find . -name ".gitignore" && find . -name ".gitmodules" ) | xargs rm -rf